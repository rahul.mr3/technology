import jenkins
import json
import re

host = "http://localhost:8080"
username = "rahulmrnaico" #jenkins username here
password = "113a35aa8955524fe4d6b40b40258eb964" # Jenkins user password / api token here
server = jenkins.Jenkins(host, username=username, password=password) #automation_user_password


#Using jenkins api get the build info
last_build_number = server.get_job_info('Technology')['lastCompletedBuild']['number']
build_info = server.get_build_info('Technology', last_build_number)
console_outputt=server.get_build_console_output("Technology", last_build_number)


#Write the data into a text file
file2 = open("file2.txt", "w+")
file2.write(console_outputt)
file2.close()


import json

commit_data = []

with open('file2.txt', 'r') as file:
    file_content = file.read()

commit_matches = re.findall(r"commit ([a-fA-F0-9]+)", file_content)
author_matches = re.findall(r"Author: (.+)\n", file_content)
date_matches = re.findall(r"Date:\s+(.*)\s+\+", file_content)

for i in range(len(commit_matches)):
    commit_id = commit_matches[i]
    author_name = author_matches[i]
    date = date_matches[i]

    commit_details = {
        "commit": commit_id,
        "author": author_name,
        "date": date
    }

    commit_data.append(commit_details)

json_data = json.dumps(commit_data, indent=4)
print(json_data)
