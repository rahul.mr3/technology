import requests
import time
import subprocess

commits_url = f"https://gitlab.com/api/v4/projects/44495862/repository/commits"
# Last commit SHA1 hash
last_commit_sha = ""

# Make a request to the GitHub API to get the latest commit information
response = requests.get(commits_url)
response_json = response.json()

# Get the SHA1 hash of the latest commit
latest_commit_sha = response_json[0]["id"]

# Check if there is a new commit

id=latest_commit_sha[:7]
print("The last commit was done by :")

p1=subprocess.Popen("git show -s --format='%an' "+ latest_commit_sha, shell=True, cwd=r"C:\ProgramData\Jenkins\.jenkins\workspace\Technology", stdout=subprocess.PIPE)
out, err = p1.communicate() 
msg1=out.decode('utf-8') 
print(msg1)
time.sleep(3)
print("Specific commit history from a defined commit to latest commit :")
p2=subprocess.Popen("git log --shortstat", shell=True, cwd=r"C:\ProgramData\Jenkins\.jenkins\workspace\Technology", stdout=subprocess.PIPE)
out, err = p2.communicate()
msg2=out.decode('utf-8')
print(msg2)
time.sleep(3)
print("")
p3=subprocess.Popen("git log --oneline", shell=True, cwd=r"C:\ProgramData\Jenkins\.jenkins\workspace\Technology", stdout=subprocess.PIPE)
out, err = p3.communicate()
msg3=out.decode('utf-8')
print(msg3)
time.sleep(3)
p1.kill()
p2.kill()
p1.kill()

print(latest_commit_sha)

# Update the last commit SHA1 hash
last_commit_sha = latest_commit_sha
